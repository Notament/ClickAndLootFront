//modules
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule} from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

//components
import { AppComponent } from './app.component';
  //account_selection
  import { AccountSelectionComponent } from './account-selection/account-selection.component';
    //sub_to_account
    import { RegisterComponent } from './register/register.component';
    import { LoginComponent } from './login/login.component';
    import { ExplanationVideoComponent } from './explanation-video/explanation-video.component';
  //hero_selection
  import { HeroSelectionComponent } from './hero-selection/hero-selection.component';
    //sub_to_hero
    import { HeroComponent } from './hero/hero.component';
    import { HeroItemComponent } from './hero-item/hero-item.component';
    import { HeroFormComponent } from './hero-form/hero-form.component';
  //game_area
  import { GameAreaComponent } from './game-area/game-area.component';
    //sub_to_game_area
    import { StuffComponent } from './stuff/stuff.component';
    import { StuffItemComponent } from './stuff-item/stuff-item.component';
    import { GridComponent } from './grid/grid.component';
    import { AthComponent } from './ath/ath.component';
    import { MonitorComponent } from './monitor/monitor.component';

//guard
import { AuthGuard } from './service/auth-guard.service';

//routes
import { routes } from './routes';

//service
import { AccountService } from './service/account.service';
import { HeroService } from './service/hero.service';
import { MapService } from './service/map.service';
import { ItemService } from './service/item.service';
import { RandomService } from './service/random.service';
import { GamePlayService } from './service/gameplay.service';



@NgModule({
  declarations: [
    AppComponent,
    AccountSelectionComponent,
    RegisterComponent,
    LoginComponent,
    ExplanationVideoComponent,
    HeroSelectionComponent,
    HeroComponent,
    HeroItemComponent,
    HeroFormComponent,
    GameAreaComponent,
    StuffComponent,
    StuffItemComponent,
    GridComponent,
    AthComponent,
    MonitorComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    HttpClientModule,
    ReactiveFormsModule,
    BrowserAnimationsModule
  ],
  providers: [
    AccountService,
    AuthGuard,
    HeroService,
    MapService,
    RandomService,
    GamePlayService,
    ItemService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
