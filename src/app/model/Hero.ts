export class Hero{
    id: number;
    user_id: number;
    pseudo: string;
    hero_class: string;
    gold: number;
    hero_X: number;
    hero_Y: number;
    health: number;
    level: number;
    current_xp: number;
    total_xp: number;
    ressource: number;

    constructor(id: number, pseudo: string, user_id :number, hero_class: string, gold: number,hero_X, hero_Y, health:number ,level:number ,current_xp:number ,total_xp:number, ressource: number ){
        this.id = id;
        this.pseudo = pseudo;
        this.user_id = user_id;
        this.hero_class = hero_class;
        this.gold = gold;
        this.hero_X = hero_X;
        this.hero_Y = hero_Y;
        this.health = health;
        this.level = level;
        this.current_xp = current_xp;
        this.total_xp = total_xp;
        this.ressource = ressource;
    }

}