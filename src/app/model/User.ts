export class User{
    id: number;
    pseudo: string;

    constructor(id: number, pseudo: string){
        this.id = id;
        this.pseudo = pseudo;
    }

}