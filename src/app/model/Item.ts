export class Item{
    id:number
	hero_id:number
	user_id:number
	complete_item:string
	name:string
	id_decoder:number
	suffixe:string
	rarity_name:string
	rarity_color:string
	STR:number
	STA:number
	AGI:number
	WIS:number
	INT:number
    LUC:number
    
    constructor(
        id:number,
        hero_id: number,
        user_id:number,
        complete_item:string,
        name:string,
        id_decoder:number,
        suffixe:string,
        rarity_name:string,
        rarity_color:string,
        STR:number,
        STA:number,
        AGI:number,
        WIS:number,
        INT:number,
        LUC:number) {
        this.id = id;
        this.hero_id = hero_id;
        this.user_id = user_id;
        this.complete_item = complete_item;
        this.name = name;
        this.id_decoder = id_decoder;
        this.suffixe = suffixe;
        this.rarity_name = rarity_name;
        this.rarity_color = rarity_color;
        this.STR = STR;
        this.STA = STA;
        this.AGI = AGI;
        this.WIS = WIS;
        this.INT = INT;
        this.LUC = LUC;
    }

}