export class Cell{

    id:number;
    X:number;
    Y:number;
    state:string;
    isDiscovered:boolean;

    constructor(X:number,Y:number,state:string,id:number = null,isDiscovered:boolean = false){
        this.id = id;
        this.X = X;
        this.Y = Y;
        this.state = state;
        this.isDiscovered = isDiscovered;
    }
}