import { Cell } from './Cell';
import { Hero } from './Hero';

export class Map{
    id: number;
    background: string;
    name:string;
    isDone: boolean;
    dbCells: Cell[] = [];
    grid: any[] = [
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    ];

    constructor(id: number, background: string, name:string, isDone:boolean){
        this.id = id;
        this.background = background;
        this.name = name;
        this.isDone = isDone;
    }

    mapGeneration(){
        let startingX = Math.floor(Math.random() * 10 );
        let startingY = Math.floor(Math.random() * 10 );
        this.grid[startingY][startingX] = 1;
        for(let howMany = 0; howMany < 25; howMany++){
            let direction;
            let lastDir;
            do{
                direction = Math.floor(Math.random() * 4 );
            } while(direction === lastDir);
            switch(direction){
                case 0:
                    lastDir = 0;
                    startingY = this.doLine('Y','decrement',startingY,startingX);
                    break;
                case 1:
                    lastDir = 1;
                    startingY = this.doLine('Y','increment',startingY,startingX);
                    break;
                case 2:
                    lastDir = 2;
                    startingX = this.doLine('X','increment',startingX,startingY);
                    break;
                case 3:
                    lastDir = 3;
                    startingX = this.doLine('X','decrement',startingX,startingY);
                    break;
            }
            //console.log(startingX,startingY);
        }
    }

    doLine(direction, crement, varDirection, secondDirection){

            let i = varDirection ;
            let iterate = Math.floor(Math.random() * 9);
            //console.log(direction,crement,varDirection,secondDirection,iterate);
            while(iterate > 0){
                iterate --;
                if(crement === "increment"){
                    i++;
                }
                else if(crement === "decrement"){
                    i--;
                }
                if(direction === "Y" && !(i > 9) && !(i < 0)){
                    this.grid[i][secondDirection] = 1;
                }
                else if(direction === "X" && !(i > 9) && !(i < 0)){
                    this.grid[secondDirection][i] = 1;
                }
            }
            if(i > 9){
                i = 9
                return i;
            }
            else if(i < 0){
                i = 0;
                return i;
            }
            else{
                return i;
            }
        }

    generatingCells(){
        let cells:Cell[] = [];
        this.grid.forEach((element, index, theArray) => {
            for (let i = 0; i < element.length ; i++) {
                if(element[i] == 1){
                    let state:string;
                    if(chance.integer({ min: 1, max: 2 }) == 1){
                        state = "fight";
                    }
                    else{
                        state = "nothing";
                    }
                    cells.push(new Cell(i,index,state))
                }
            }
        })
        return cells;
    }

    cellOnGrid(cells,hero){
        cells.forEach((cell) => {
            if(cell.state === "fight"){
                this.grid[cell.Y][cell.X] = 1;
            }
            if(cell.state === "nothing"){
                this.grid[cell.Y][cell.X] = 2;
            }
            if(cell.isDiscovered == 1){
                this.grid[cell.Y][cell.X] = 3;
            }
        })
        if(hero !== undefined && hero.hero_Y !== null && hero.hero_X != null){
            this.grid[hero.hero_Y][hero.hero_X] = 4;
        }

    }

    cellExist(X,Y){
        if( X < 10 && X >= 0 && Y < 10 && Y >= 0 && this.grid[Y][X] != 0){
            return true;
        }
        return false;
    }


}