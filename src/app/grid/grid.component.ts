import { Component, OnInit } from '@angular/core';
import { MapService } from '../service/map.service';
import { Map } from '../model/Map';
import { Hero } from '../model/Hero';

@Component({
  selector: 'grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.scss'],
})
export class GridComponent implements OnInit {

  map:Map;
  hero: Hero;

  constructor(private mapService: MapService) { }

  ngOnInit() {
    this.mapService.subjectMap.subscribe(
      (map) => {
        this.map = map;
      }
    )
    this.hero = this.mapService.hero;
  }
}
