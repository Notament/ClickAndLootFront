import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExplanationVideoComponent } from './explanation-video.component';

describe('ExplanationVideoComponent', () => {
  let component: ExplanationVideoComponent;
  let fixture: ComponentFixture<ExplanationVideoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExplanationVideoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExplanationVideoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
