import { Component, OnInit } from '@angular/core';
import { MapService } from '../service/map.service';
import { Map } from '../model/Map';

@Component({
  selector: 'app-game-area',
  templateUrl: './game-area.component.html',
  styleUrls: ['./game-area.component.scss']
})
export class GameAreaComponent implements OnInit {

  map:Map;

  constructor(private mapService: MapService) { }

  ngOnInit() {
    this.mapService.subjectMap.subscribe(
      (map) => {
        this.map = map;
      }
    )
  }

}
