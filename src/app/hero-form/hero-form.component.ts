import { Component, OnInit } from '@angular/core';
import { AccountService } from '../service/account.service';
import { HeroService } from '../service/hero.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { User } from '../model/User';

@Component({
  selector: 'hero-form',
  templateUrl: './hero-form.component.html',
  styleUrls: ['./hero-form.component.scss']
})
export class HeroFormComponent implements OnInit {

  private user:User;
  private addForm: FormGroup;
  private doneMessage : string;

  constructor(
    private formBuilder: FormBuilder,
    private accountService: AccountService,
    private heroService: HeroService) {}

    ngOnInit() {
      this.initForm();
      this.accountService.userSubject.subscribe(
        (user) => {
          this.user = user;
        }
      )

      this.heroService.createHeroSubject.subscribe(
        (message) => {
          this.doneMessage = message;
        }
      )
    }

    initForm() {
      this.addForm = this.formBuilder.group({
        pseudo: ['', Validators.required],
        hero_class: ['', Validators.required],
      });
    }

    addHero(){
      const pseudo = this.addForm.get('pseudo').value;
      const hero_class = this.addForm.get('hero_class').value;
      this.heroService.addHero(pseudo,hero_class,this.user.id);
    }

}
