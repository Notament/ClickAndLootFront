import { Component, OnInit, ViewChild } from '@angular/core';
import { MapService } from '../service/map.service';
import { ItemService } from '../service/item.service';
import { Map } from '../model/Map';
import { Hero } from '../model/Hero';
import { Cell } from '../model/Cell';
import { Item } from '../model/Item';
import {
  trigger,
  state,
  style,
  animate,
  transition,
} from '@angular/animations';


@Component({
  selector: 'monitor',
  templateUrl: './monitor.component.html',
  styleUrls: ['./monitor.component.scss'],
  animations:[
    //Hero Walk
    trigger('walkHero', [
      state('void',style({
        position: 'absolute', left: '0%',bottom : 0
      })),
      state('nothing,looting_kill',
        style({ display:'block', position: 'absolute', left: '35%', bottom : 0})
      ),
      state('fight,attack,death',
      style({ display:'block', position: 'absolute', left: '20%', bottom : 0 })
      ),
      transition('* => *', animate('2s linear')),
      transition('* => fight', animate('2s linear'))
    ]),

    //Monter Walk
    trigger('MonsterWalk', [
      state('void,death,looting,looting_kill',style({
        position: 'absolute', right: '0%',display : 'none'
      })),
      state('fight,attack',
      style({ position: 'absolute', right: '20%' })
      ),
      transition('void => fight', animate('2s linear'))
    ]),

    //Death Anim
    trigger('DeathLoop', [
      state('void,fight,attack,looting,looting_kill',style({
        position: 'absolute', left: '{{ monsterPos }}px', display:'none'
      }),{params : {monsterPos : 0}}),
      state('death',
      style({ display:'block', position: 'absolute', left: '{{ monsterPos }}px' ,bottom : 0})
      ,{params : {monsterPos : 0}}),
      transition('* => nothing', animate('0.4s linear'))
    ]),

    //Spell Throw
    trigger('Spellthrow', [
      state('void,fight,death,looting_kill,looting',style({
       position: 'absolute', left: '{{ heroPos }}px', bottom : 0, display: 'none'
      }),{params : {heroPos : 0}},), 
      state('attack',
      style({ display:'block', position: 'absolute', left: '{{ monsterPos }}px', bottom : 0 }),
      {params : {monsterPos : 0}}),
      transition('* => attack', animate('2s linear'))
    ]),

    //Chest Anim
    trigger('chestLoop', [
      state('void,fight,attack,death',style({
        display: 'none', position: 'absolute', left: '40%', bottom : 0
      })),
      state('looting,looting_kill,nothing', 
      style({ display : 'block', position: 'absolute', left: '40%', bottom : 0})
      ),
      transition('* => looting', animate('2s linear'))
    ]),

    //Item Display
    trigger('PrintItem', [
      state('void,nothing,fight,attack,death,looting_kill',style({
        display: 'none'
      })),
      state('looting,',
      style({ display : 'block', position: 'absolute'})
      ),
      transition('* => looting', animate('2s linear'))
    ]),

    //
  ]
})
export class MonitorComponent implements OnInit {

  //Model
  generatedItem:Item;
  items:Item[];
  map:Map;
  hero: Hero;
  cell: Cell;
  diffStats;

  //State
  state: string = 'void'
  isClicked: boolean = true;
  itemAction: boolean;
  hasBeenLooted:boolean;

  //Dom Element
  @ViewChild('myHero') divHero;
  @ViewChild('myMonster') divMonster;
  monsterPos = 0;
  heroPos = 0;

  //Sprite
  spriteHero: string = 'stand_right.png';
  spriteChest: string = 'Chest_left.png';
  monster = {
    action : 'stand',
    color : this.monsterColor(),
    direction: 'left',
    extension: 'png'
  }

  constructor(private mapService: MapService, private itemService: ItemService) { }

  ngOnInit() {
    this.hero = this.mapService.hero;

    this.itemService.subjectItem.subscribe(
      (item) => {
        this.generatedItem = item;
      }
    )

    this.itemService.subjectItem.subscribe(
      (item) => {
        this.generatedItem = item;
      }
    )

    this.itemService.subjectDiffStat.subscribe(
      (diffStats) => {
        this.diffStats = diffStats;
      }
    )

    this.itemService.subjectAllItems.subscribe(
      (items) => {
        this.items = items;
      }
    )

    this.mapService.subjectMap.subscribe(
      (map) => {
        this.cell = map.dbCells.find((cell) =>{
          if(cell.X == this.hero.hero_X && cell.Y == this.hero.hero_Y){
              return cell;
          }
        });
        this.cell = this.cell != undefined ? this.cell : new Cell(null,null,null,null,true);
        this.hasBeenLooted = this.cell.isDiscovered;
        this.itemAction = Boolean(this.cell.isDiscovered);
      }
    )
  }

  loot(state) {
    this.isClicked = false;
    this.state = state;
    this.itemService.generateAnItemType();

  }

  startAnim(event){
    this.spriteShow(event);
  }

  doneAnim(event){
    this.spriteShow(event);
  }

  spriteShow(event){
    switch(true){
      case event.phaseName == 'start' && event.triggerName == 'walkHero' && event.toState != 'void' && this.state == 'nothing':
        this.spriteHero = "walk_right.gif";
        this.spriteChest = 'Chest_left.png';
      break;
      case event.phaseName == 'done' && event.triggerName == 'walkHero' && event.toState != 'void' && this.state == 'nothing' :
        this.spriteHero = "attack_right.gif";
        this.spriteChest = "Opening_chest_left.gif";
        this.state = 'looting';
      break;
      case event.phaseName == 'start' && event.triggerName == 'walkHero' && event.toState != 'void' && this.state == 'fight':
        this.spriteHero = "walk_right.gif";
        this.monster.action = 'Walk'; 
        this.monster.extension = 'gif';
      break;
      case event.phaseName == 'done' && event.triggerName == 'walkHero' && event.toState != 'void' && this.state == 'fight':
        this.spriteHero = "attack_right.gif";
        this.monster.action = 'Attack';

        this.divHero = HTMLElement = this.divHero.nativeElement;
        this.divMonster = HTMLElement = this.divMonster.nativeElement;

        this.heroPos = this.divHero.offsetLeft;
        this.monsterPos = this.divMonster.offsetLeft;
        setTimeout(()=>{this.state = 'attack'},100)
      break;
      case event.phaseName == 'done' && event.triggerName == 'Spellthrow' && event.toState != 'void' && this.state == 'attack':
        setTimeout(()=>{this.state = 'death'},100)
      break;
      case event.phaseName == 'done' && event.triggerName == 'DeathLoop' && event.toState != 'void' && this.state == 'death':
        this.spriteHero = "walk_right.gif";
        this.spriteChest = 'Chest_left.png';
        setTimeout(()=>{this.state = 'looting_kill'},400)
      break;
      case event.fromState == "death" && event.phaseName == 'done' && event.triggerName == 'walkHero' && event.toState != 'void' && this.state == 'looting_kill':
         this.spriteHero = "attack_right.gif";
         this.spriteChest = "Opening_chest_left.gif";
         this.state = "looting";
      break;
    }
  }

  looting(item) {
    //Dom
    this.isClicked = true;
    this.state = 'void';

    //Service
    this.itemService.addItemtoHero(item);
    this.mapService.cellDiscovery(this.cell.X,this.cell.Y);

  }

  throw() {
    //Dom
    this.isClicked = true;
    this.state = 'void';
    
    //Service
    this.mapService.cellDiscovery(this.cell.X,this.cell.Y);
  }

  monsterColor(){
    switch(chance.integer({ min: 1, max: 3})) {
      case 1:
        return 'red'
      case 2:
        return 'blue'
      case 3:
        return 'yellow'
    }
  }
}
