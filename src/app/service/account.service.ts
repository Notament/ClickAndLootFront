import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { User } from "../model/User";
import { Subject } from 'rxjs/Subject';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Router } from "@angular/router";


@Injectable()
export class AccountService {

    //config
    private simplonApi: string = "http://192.168.1.37/ChefChef/ClickAndLoot_Api_Framework/public";
    private homeApi: string = "http://192.168.1.28/ChefChef/ClickAndLoot_Api_Framework/public";
    private nijiApi: string = "http://10.244.2.89/ChefChef/ClickAndLoot_Api_Framework/public";
    private dockerApi: string = "http://localhost:20000";

    //message
    private messageDoneRegister:string;
    private messageDoneLogin:string;

    //model
    private user:User;

    public isAuth:boolean = false;

    //Subject
    public registerSubjectMessage:Subject<string>  = new Subject<string>()
    public loginSubjectMessage:Subject<string>  = new Subject<string>()
    public userSubject:Subject<User>  = new BehaviorSubject<User>(new User(null,null));

    constructor(
        private http: HttpClient,
        private router: Router
    ){}

    emitRegisterMessages(){
        this.registerSubjectMessage.next(this.messageDoneRegister);
    }

    emitLoginMessages(){
        this.loginSubjectMessage.next(this.messageDoneLogin);
    }

    emitUser(){
        this.userSubject.next(this.user);
    }

    register(pseudo,password){
        let observable = this.http.post<any>(`${this.dockerApi}/user/new`,{'pseudo': pseudo, 'password': password});
        let observer = {
            next: (data) => {
                this.messageDoneRegister = data.message;
                this.emitRegisterMessages();
            },
            error: (error) => { console.log(error) },
            complete: () => { console.log() }
        }

        observable.subscribe(observer);
    }

    login(pseudo,password){
        let observable = this.http.post<any>(`${this.dockerApi}/user/login`,{'pseudo': pseudo, 'password': password});
        let observer = {
            next: (data) => {
                if(data.message){
                    this.messageDoneLogin = data.message;
                    this.emitLoginMessages();
                }
                else{
                    this.user = new User(data.id,data.pseudo);
                    this.emitUser();
                    this.messageDoneLogin = 'Compte valide, login en cours';
                    this.emitLoginMessages();
                    this.isAuth = true;
                }
            },
            error: (error) => { console.log(error) },
            complete: () => { this.router.navigate(["/hero"]); }
        }

        observable.subscribe(observer);
    }

}