import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs/Subject';
import { MapService } from "./map.service";
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class RandomService {

    private simplonApi: string = "http://192.168.1.37/ChefChef/ClickAndLoot_Api_Framework/public";
    private homeApi: string = "http://192.168.1.28/ChefChef/ClickAndLoot_Api_Framework/public";


    constructor(
        private http: HttpClient,
        private mapService: MapService
    ){}

}
