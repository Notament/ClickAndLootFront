import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs/Subject';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Router } from "@angular/router";
import { Hero } from "../model/Hero";
import { Item } from "../model/Item";
import { share } from 'rxjs/operators';
import { HeroService } from './hero.service';

@Injectable()
export class ItemService {

    constructor(
        private http: HttpClient,
        private heroService: HeroService,
        // private router: Router
    ){
        this.heroService.heroSubject.subscribe(
            (hero) => {
                this.hero = hero;
            }
        )
    }

    private simplonApi: string = "http://192.168.1.37/ChefChef/ClickAndLoot_Api_Framework/public";
    private homeApi: string = "http://192.168.1.28/ChefChef/ClickAndLoot_Api_Framework/public";
    private nijiApi: string = "http://10.244.2.89/ChefChef/ClickAndLoot_Api_Framework/public";
    private dockerApi: string = "http://localhost:20000";

    hero: Hero;
    items: Item[] = [];
    diffStats = {};
    generatedItem: Item;
    reducer = (accumalotor, currentValue) => accumalotor + currentValue;

    subjectAllItems: Subject<Item[]> = new BehaviorSubject<Item[]>(new Item(null,null,null,null,null,null,null,null,null,null,null,null,null,null,null));
    subjectItem: Subject<Item>= new Subject<Item>();
    subjectDiffStat: Subject<any>= new Subject<any>();

    emitItem(){
        this.subjectItem.next(this.generatedItem);
    }

    emitItems(){
        this.subjectAllItems.next(this.items);
    }

    emitDiffStat(){
        this.subjectDiffStat.next(this.diffStats);
    }



    getAllItems(id_hero) {
        let observable = this.http.get<any>(`${this.dockerApi}/item/all/${id_hero}`);

        let observer = {
            next: (data) => {
                this.items = [];
                if(Object.keys(data).length === 0 && data.constructor === Object){
                    return;
                }
                data.forEach(item => {
                    this.items.push(new Item(
                        item.id,
                        this.hero.id,
                        this.hero.user_id,
                        item.complete_item,
                        item.name,
                        item.id_decoder,
                        item.suffixe,
                        item.rarity_name,
                        item.rarity_color,
                        item.str,
                        item.sta,
                        item.agi,
                        item.wis,
                        item.intel,
                        item.luc
                        ))
                });
            },
            error: (error) => { console.log(error) },
            complete: () => { this.emitItems() }
        }

        observable.subscribe(observer);
    }

    addItemtoHero(item) {
        let observable = this.http.post<any>(`${this.dockerApi}/item/new`,{
        "hero_id": item.hero_id,
        "user_id": item.user_id,
        "complete_item": item.complete_item,
        "name": item.name,
        "id_decoder": item.id_decoder,
        "suffixe": item.suffixe,
        "rarity_name": item.rarity_name,
        "rarity_color": item.rarity_color,
        "STR": item.STR,
        "STA": item.STA,
        "AGI": item.AGI,
        "WIS": item.WIS,
        "INT": item.INT,
        "LUC": item.LUC,    
        });

        let observer = {
            next: () => { },
            error: (error) => { console.log(error) },
            complete: () => { this.getAllItems(this.hero.id,this.hero) }
        }

        observable.subscribe(observer);
    }


                                                                        //ITEM GEN
    generateAnItemType() {
        let itemTypeId = chance.integer({ min: 1, max: 13 });
        let observable;
        switch(itemTypeId) {
            case 1:
                let weaponLeftId = chance.integer({ min: 1, max: 12 });
                observable = this.http.get<any>(`${this.dockerApi}/random/left_weapon/${weaponLeftId}`)
                break;
            case 2:
                let weaponRightId = chance.integer({ min: 1, max: 4 });
                observable = this.http.get<any>(`${this.dockerApi}/random/right_weapon/${weaponRightId}`)
                break;
            default:
                observable = this.http.get<any>(`${this.dockerApi}/random/item_type/${itemTypeId}`)
        }
        let observer = {
            next: (data) => {
                this.generatedItem = 
                new Item(null,
                        this.hero.id,
                        this.hero.user_id,
                        null,
                        data.name,
                        itemTypeId,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null
                    )
            },
            error: (error) => { console.log(error) },
            complete: () => { this.generateSuffixe() }
        }
        observable.subscribe(observer);
    }
    
                                                                        //ITEM GEN
                                                                        
    generateSuffixe() {
        let suffixeId = chance.integer({ min: 1, max: 18 });
        let observable = this.http.get<any>(`${this.dockerApi}/random/suffixe_item/${suffixeId}`)
        let observer = {
            next: (data) => {
                this.generatedItem.suffixe = data.name;
            },
            error: (error) => { console.log(error) },
            complete: () => { this.generateStatsAndRarity() }
        }

        observable.subscribe(observer);
    }

                                                                            //ITEM GEN

    generateStatsAndRarity() {
        let rarity = this.generateRarity();
        let minimalValue = this.heroGrowthStat();
        if(minimalValue <= 6){
            this.generatedItem.STR = Math.round(chance.integer({ min: minimalValue, max: minimalValue + 5  }) + this.generatedItem.STR * rarity.index);
            this.generatedItem.STA = Math.round(chance.integer({ min: minimalValue, max: minimalValue + 5  }) + this.generatedItem.STR * rarity.index);
            this.generatedItem.AGI = Math.round(chance.integer({ min: minimalValue, max: minimalValue + 5  }) + this.generatedItem.STR * rarity.index);
            this.generatedItem.WIS = Math.round(chance.integer({ min: minimalValue, max: minimalValue + 5  }) + this.generatedItem.STR * rarity.index);
            this.generatedItem.INT = Math.round(chance.integer({ min: minimalValue, max: minimalValue + 5  }) + this.generatedItem.STR * rarity.index);
            this.generatedItem.LUC = Math.round(chance.integer({ min: minimalValue, max: minimalValue + 5  }) + this.generatedItem.STR * rarity.index);
        }
        else {
            this.generatedItem.STR = Math.round(chance.integer({ min: minimalValue - 5, max: minimalValue + 5  }) + this.generatedItem.STR * rarity.index);
            this.generatedItem.STA = Math.round(chance.integer({ min: minimalValue - 5, max: minimalValue + 5  }) + this.generatedItem.STR * rarity.index);
            this.generatedItem.AGI = Math.round(chance.integer({ min: minimalValue - 5, max: minimalValue + 5  }) + this.generatedItem.STR * rarity.index);
            this.generatedItem.WIS = Math.round(chance.integer({ min: minimalValue - 5, max: minimalValue + 5  }) + this.generatedItem.STR * rarity.index);
            this.generatedItem.INT = Math.round(chance.integer({ min: minimalValue - 5, max: minimalValue + 5  }) + this.generatedItem.STR * rarity.index);
            this.generatedItem.LUC = Math.round(chance.integer({ min: minimalValue - 5, max: minimalValue + 5  }) + this.generatedItem.STR * rarity.index);
        }
        this.generatedItem.rarity_name = rarity.name;
        this.generatedItem.rarity_color = rarity.color;
        this.generatedItem.complete_item = rarity.name +' '+ this.generatedItem.name +' '+ this.generatedItem.suffixe;

        this.emitItem();
        this.diffStatsGenerate();


    }

                                                                            //ITEM GEN

    heroGrowthStat() {
        let stats = [];
        if(this.items.length != 0) {
            this.items.forEach((item) => {
                stats.push(parseInt(item.STA),parseInt(item.AGI),parseInt(item.STR),parseInt(item.LUC),parseInt(item.INT),parseInt(item.WIS));
            })
            return stats.reduce(this.reducer) / stats.length;
        }
        return 1;
    }

                                                                            //ITEM GEN

    generateRarity() {
        let rarityOne = chance.integer({ min: 1, max: 20000 });
        let rarityTwo = chance.integer({ min: 1, max: 20000 });
    
        if (rarityOne == rarityTwo) {
                return {name: "????", color: "#000000", index: 1};
            }
        else if (rarityOne != rarityTwo) {
            if (rarityTwo - 20 < rarityOne && rarityTwo + 20 > rarityOne) {
                return {name: "demonic", color: "#ff0000", index: 0.5};
            }
            else if (rarityTwo - 100 < rarityOne && rarityTwo + 100 > rarityOne) {
                return {name: "mystic", color: "#00ffd8", index: 0.25};
            }
            else if (rarityTwo - 200 < rarityOne && rarityTwo + 200 > rarityOne) {
                return {name: "legendary", color: "#ff6600", index: 0.1};
            }
            else if (rarityTwo - 1000 < rarityOne && rarityTwo + 1000 > rarityOne) {
                return {name: "epic", color: "#6600cc", index: 0.075};
            }
            else if (rarityTwo - 2000 < rarityOne && rarityTwo + 2000 > rarityOne) {
                return {name: "rare", color: "#ffff66", index: 0.05};
            }
            else if (rarityTwo - 3000 < rarityOne && rarityTwo + 3000 > rarityOne) {
                return {name: "magic", color: "#0000ff", index: 0.035};
            }
            else if (rarityTwo - 5000 < rarityOne && rarityTwo + 5000 > rarityOne) {
                return {name: "uncomon", color: "#00ff00", index: 0.025};
            }
            else {
                return {name: "common", color: "#ffffff", index: 0.01};
            }
        } 
    }

    diffStatsGenerate() {
        
        this.items.forEach(item => {
          if(item.id_decoder == this.generatedItem.id_decoder){
            this.diffStats.STR = this.signMath(item,this.generatedItem,'STR');
            this.diffStats.signSTR = Math.sign(parseInt(this.diffStats.STR));
            this.diffStats.STA = this.signMath(item,this.generatedItem,'STA');
            this.diffStats.signSTA = Math.sign(parseInt(this.diffStats.STA));
            this.diffStats.WIS = this.signMath(item,this.generatedItem,'WIS');
            this.diffStats.signWIS = Math.sign(parseInt(this.diffStats.WIS));
            this.diffStats.INT = this.signMath(item,this.generatedItem,'INT');
            this.diffStats.signINT = Math.sign(parseInt(this.diffStats.INT));
            this.diffStats.LUC = this.signMath(item,this.generatedItem,'LUC');
            this.diffStats.signLUC = Math.sign(parseInt(this.diffStats.LUC));
            this.diffStats.AGI = this.signMath(item,this.generatedItem,'AGI');
            this.diffStats.signAGI = Math.sign(parseInt(this.diffStats.AGI));
          }
        })
        if(this.items.length == 0 || this.diffStats == undefined){
            this.diffStats.STR = '+'+ this.generatedItem.STR;
            this.diffStats.STA = '+'+ this.generatedItem.STA;
            this.diffStats.WIS = '+'+ this.generatedItem.WIS;
            this.diffStats.INT = '+'+ this.generatedItem.INT;
            this.diffStats.LUC = '+'+ this.generatedItem.LUC;
            this.diffStats.AGI = '+'+ this.generatedItem.AGI;
            this.diffStats.signSTR = Math.sign(parseInt(this.diffStats.STR));
            this.diffStats.signSTA = Math.sign(parseInt(this.diffStats.STA));
            this.diffStats.signWIS = Math.sign(parseInt(this.diffStats.WIS));
            this.diffStats.signINT = Math.sign(parseInt(this.diffStats.INT));
            this.diffStats.signLUC = Math.sign(parseInt(this.diffStats.LUC));
            this.diffStats.signAGI = Math.sign(parseInt(this.diffStats.AGI));
        }
        this.emitDiffStat();
    }

    signMath(item,itemG,stat){
        let max = Math.max(parseInt(item[stat]),parseInt(itemG[stat]));
        let min = Math.min(parseInt(item[stat]), parseInt(itemG[stat]));
        let result = parseInt(item[stat]) - parseInt(itemG[stat])
        switch(Math.sign(result)) {
            case 1:
                return '-' + Math.abs(result);
            case -1:
                return '+'+ Math.abs(result);
            case 0:
                return '+ 0';
        }
    }

}