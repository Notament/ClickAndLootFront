import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs/Subject';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Router } from "@angular/router";
import { HeroService } from './hero.service';
import { Map } from "../model/Map";
import { Cell } from "../model/Cell";
import { Hero } from "../model/Hero";
import { share } from 'rxjs/operators';

@Injectable()
export class MapService {

    private simplonApi: string = "http://192.168.1.37/ChefChef/ClickAndLoot_Api_Framework/public";
    private homeApi: string = "http://192.168.1.28/ChefChef/ClickAndLoot_Api_Framework/public";
    private nijiApi: string = "http://10.244.2.89/ChefChef/ClickAndLoot_Api_Framework/public";
    private dockerApi: string = "http://localhost:20000";

    map: Map;
    hero: Hero;
    mapInfo: Object;
    isClicked: boolean = true;

    subjectisClicked: Subject<boolean> = new BehaviorSubject<boolean>(true);
    subjectMap: Subject<Map> = new BehaviorSubject<Map>(new Map(null,null,null,null));
    subjectHero

    constructor(
        private http: HttpClient,
        private router: Router,
        private heroService: HeroService,
    ){
        this.heroService.heroSubject.subscribe(
            (hero) => {
                this.hero = hero;
            }
        )
    }

    emitMap(){
        this.subjectMap.next(this.map);
    }

    emitIsClicked(){
        this.subjectisClicked.next(this.isClicked);
    }

    getAllMaps(id_hero){
        let observable = this.http.get<any>(`${this.dockerApi}/map/all/${id_hero}`);
        let observer = {
            next: (data) => {

                if(data.message || this.doesItContainThisKey(data)){
                    this.randomizeElementsMap(id_hero);
                }
                else{
                    this.map = new Map(data.mapToDo.id,data.mapToDo.background,data.mapToDo.name,data.mapToDo.isDone);
                    this.getCells(this.map.id);
                }
            },
            error: (error) => { console.log(error) },
            complete: () => { console.log() }
        }

        observable.subscribe(observer);
    }

    randomizeElementsMap(id_hero){
        let id_sf = chance.integer({ min: 1, max: 10 });
        let id_env = chance.integer({ min: 1, max: 10 });
        let observable = this.http.get<any>(`${this.dockerApi}/random/suffixe_map/${id_sf}/environnement/${id_env}`);
        let observer = {
            next: (data) => {
                this.newMap(data[0].name +' '+ data[1].name ,data[0].background, id_hero)
            },
            error: (error) => { console.log(error) },
            complete: () => { console.log() }
        }

        observable.subscribe(observer);
    }

    newMap(name,background,id_hero){
        let observable = this.http.post<any>(`${this.dockerApi}/map/new`,{"name": name, "background": background, "hero_id": id_hero});
        let observer = {
            next: () => {},
            error: (error) => { console.log(error) },
            complete: () => { this.getAllMaps(id_hero) }
        }

        observable.subscribe(observer);
    }

    doesItContainThisKey(data){
        let result:boolean;
        Object.keys(data).some((key) => {
            if (key.match(/^map[0-9]+/) != null) {
                result = true;
            }
            else{
                result = false;
            }
        });
        return result;
    }

    getCells(id_map){
        let count = 0;
        let observable = this.http.get<any>(`${this.dockerApi}/cell/all/${id_map}`);
        let observer = {
            next: (data) => {
                if(data.length === 0){
                    this.map.mapGeneration();
                    let cells:Cell[] = this.map.generatingCells();
                    const total = cells.length;
                    cells.forEach((cell) => {
                        this.createCell(id_map,cell.X,cell.Y,cell.state).subscribe(
                            () => {
                                count++;
                                if (count >= total) {
                                    this.getCells(id_map);
                                }
                             });
                    })
                }
                else{
                    this.map.dbCells = [];
                    data.forEach((cell) => {
                        this.map.dbCells.push(new Cell(cell.x_coord,cell.y_coord,cell.state,cell.id,cell.is_discovered));
                    })
                    this.map.cellOnGrid(this.map.dbCells,this.hero);
                    this.HeroOnGrid();
                    this.emitMap();
                    this.emitIsClicked();
                    this.isMapDone();
                    // console.log(this.map.grid);
                }
            },
            error: (error) => { console.log(error) },
            complete: () => { this.router.navigate(["/game"])}
        }

        observable.subscribe(observer);
    }

    createCell(id_map,X,Y,state){
        let observable = this.http.post<any>(`${this.dockerApi}/cell/new`,{"map_id": id_map, "X": X, "Y": Y, "state":state}).pipe(share());
        let observer = {
            next: () => {},
            error: (error) => { console.log(error) },
            complete: () => {}
        }

        observable.subscribe(observer);
        return observable;
    }

    HeroOnGrid(){
        if(this.hero.hero_X == null && this.hero.hero_Y == null){
            let cell: Cell;
            do{
                cell = this.map.dbCells[chance.integer({ min: 0, max: this.map.dbCells.length })]
            }
            while(cell.state === "fight");
            this.moveHeroOnGrid(cell.X,cell.Y);
        }
    }

    moveHeroOnGrid(X,Y){
        this.isClicked = false;
        this.emitIsClicked();
        this.heroService.setHeroCoords(X,Y);
        this.heroService.coordSubject.subscribe(
            () => {
                let cell = this.map.dbCells.find((cell) => {
                    if(cell.X == X && cell.Y == Y) {
                        return cell;
                    }
                })
                this.map.cellOnGrid(this.map.dbCells,this.hero);
                this.emitMap();
                //console.log(this.map.dbCells.every(this.isDone));
                if(cell.isDiscovered == true){
                    this.isClicked = true;
                    this.emitIsClicked();
                }
            }
        )
    }

    cellDiscovery(X,Y){
        let cell = this.map.dbCells.find((cell) => {
            if(cell.X == X && cell.Y == Y) {
                return cell;
            }
        })
        if(cell.isDiscovered == false){
            let observable = this.http.get<any>(`${this.dockerApi}/cell/done/${cell.id}`);
            let observer = {
                next: () => {
                    this.getCells(this.map.id);
                    this.isClicked = true;
                    this.emitIsClicked();
                },
                error: (error) => { console.log(error) },
                complete: () => {}
            }

            observable.subscribe(observer);
        }
    }

    isMapDone(){
        if(this.map.dbCells.every(this.isDone)) {
            let observable = this.http.patch<any>(`${this.dockerApi}/map/done/${this.map.id}`);
            let observer = {
                next: () => {
                    this.heroService.setHeroCoords(null,null);
                },
                error: (error) => { console.log(error) },
                complete: () => { this.getAllMaps(this.hero.id) }
            }

            observable.subscribe(observer);
        }
        else {
            return console.log('Keep crawling in this area adventurer') 
        }
    }

    isDone(cell){
        return cell.isDiscovered == 1;
    }
}

