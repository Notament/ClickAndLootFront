import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
//import { User } from "../model/User";
import { Hero } from "../model/Hero";
import { Subject } from 'rxjs/Subject';
//import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Router } from "@angular/router";


@Injectable()
export class HeroService {

    //config
    private simplonApi: string = "http://192.168.1.37/ChefChef/ClickAndLoot_Api_Framework/public";
    private homeApi: string = "http://192.168.1.28/ChefChef/ClickAndLoot_Api_Framework/public";
    private nijiApi: string = "http://10.244.2.89/ChefChef/ClickAndLoot_Api_Framework/public";
    private dockerApi: string = "http://localhost:20000";

    messageDoneCreate:string;
    messageDoneDelete:string;
    heroes: Hero[] = [];
    currentHero: Hero;

    //subject
    createHeroSubject: Subject<string> = new Subject<string>();
    deleteHeroSubject: Subject<string> = new Subject<string>();
    heroesSubject: Subject<Hero[]> = new Subject<Hero[]>()
    heroSubject: Subject<Hero> = new Subject<Hero>();
    coordSubject: Subject<Hero> = new Subject<Hero>()

    constructor(
        private http: HttpClient,
        private router: Router
    ){}

    emitCreateHeroMessages(){
        this.createHeroSubject.next(this.messageDoneCreate);
    }

    emitDeleteHeroMessages(){
        this.deleteHeroSubject.next(this.messageDoneDelete);
    }

    emitHeroes(){
        this.heroesSubject.next(this.heroes);
    }

    emitHero(){
        this.heroSubject.next(this.currentHero);
    }

    setHero(hero){
        this.currentHero = hero;
        this.emitHero();
    }

    addHero(pseudo,hero_class,user_id){
        let observable = this.http.post<any>(`${this.dockerApi}/hero/new`,{'pseudo': pseudo, 'hero_class': hero_class, 'user_id':user_id});
        let observer = {
            next: (data) => {
                this.messageDoneCreate = data.message;
                this.emitCreateHeroMessages();
            },
            error: (error) => { console.log(error) },
            complete: () => { this.getAllHero(user_id) }
        }

        observable.subscribe(observer);
    }

    getAllHero(user_id){

        let observable = this.http.get<any>(`${this.dockerApi}/hero/all/${user_id}`);
        let observer = {
            next: (data) => {
                this.heroes = [];
                data.forEach((hero) => {
                    this.heroes.push(new Hero(
                        hero.id,
                        hero.pseudo,
                        hero.user_id,
                        hero.hero_class,
                        hero.gold,
                        hero.current_x,
                        hero.current_y,
                        hero.health,
                        hero.level,
                        hero.current_xp,
                        hero.total_xp,
                        hero.ressource));
                });
                this.emitHeroes();
            },
            error: (error) => { console.log(error) },
            complete: () => { console.log() }
        }

        observable.subscribe(observer);
    }

    deleteHero(id,user_id){
        let observable = this.http.delete<any>(`${this.dockerApi}/hero/delete/${id}`);
        let observer = {
            next: (data) => {
                this.messageDoneDelete = data.message;
                this.emitDeleteHeroMessages();
            },
            error: (error) => { console.log(error) },
            complete: () => { this.getAllHero(user_id) }
        }

        observable.subscribe(observer);
    }

    setHeroCoords(X,Y){
        this.currentHero.hero_X = X;
        this.currentHero.hero_Y = Y;
        this.emitHero();
        this.http.patch<any>(`${this.dockerApi}/hero/set/position/${this.currentHero.id}`,{'X':X,'Y':Y})
            .subscribe(
                () => { this.coordSubject.next() },
                error => { console.log(error)}
            )
    }
}