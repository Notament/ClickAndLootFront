import { Component, OnInit, Input } from '@angular/core';
import { Item } from '../model/Item';

@Component({
  selector: 'stuff-item',
  templateUrl: './stuff-item.component.html',
  styleUrls: ['./stuff-item.component.scss']
})
export class StuffItemComponent implements OnInit {

  @Input() item: Item;
  mouseX: string;
  mouseY: string;

  constructor() { }

  ngOnInit() {
  }

  mousePos(e){
    //console.log(e);
    this.mouseX = e.clientX + 20 + 'px';
    this.mouseY = e.clientY + 'px';
  }

}
