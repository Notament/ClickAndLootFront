import { Component, OnInit } from '@angular/core';
import { AccountService } from '../service/account.service';
import { User } from '../model/User';

@Component({
  selector: 'app-hero-selection',
  templateUrl: './hero-selection.component.html',
  styleUrls: ['./hero-selection.component.scss']
})
export class HeroSelectionComponent implements OnInit {

  private user:User;

  constructor(
    private accountService: AccountService) {}


  ngOnInit() {
    this.accountService.userSubject.subscribe(
      (user) => {
        this.user = user;
      }
    )
  }

}
