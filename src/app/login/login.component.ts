import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { AccountService } from '../service/account.service';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(
    private formBuilder: FormBuilder,
    private accountService: AccountService,

  ) { }

  private addForm: FormGroup;
  private doneMessage : string;

  ngOnInit() {
    this.initForm();
    this.accountService.loginSubjectMessage.subscribe(
      (message) => {
        this.doneMessage = message;
      }
    )

  }

  initForm() {
    this.addForm = this.formBuilder.group({
      pseudo: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  login(){
    const pseudo = this.addForm.get('pseudo').value;
    const password = this.addForm.get('password').value;
    this.accountService.login(pseudo,password);
  }
}
