import { Routes } from '@angular/router';
import { AccountSelectionComponent } from './account-selection/account-selection.component';
import { HeroSelectionComponent } from './hero-selection/hero-selection.component';
import { GameAreaComponent } from './game-area/game-area.component';
import { AuthGuard} from './service/auth-guard.service';


export const routes: Routes = [
    { path: 'account', component: AccountSelectionComponent},
    { path: 'hero', canActivate:[AuthGuard], component: HeroSelectionComponent},
    { path: 'game', canActivate:[AuthGuard], component: GameAreaComponent},
    { path: '', component: AccountSelectionComponent},
    { path: '**', redirectTo: '' }

]