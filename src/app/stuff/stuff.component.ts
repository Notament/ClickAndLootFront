import { Component, OnInit } from '@angular/core';
import { ItemService } from '../service/item.service';
import { Item } from '../model/Item';

@Component({
  selector: 'stuff',
  templateUrl: './stuff.component.html',
  styleUrls: ['./stuff.component.scss']
})
export class StuffComponent implements OnInit {

  items:Item[] = [];

  constructor(private itemService: ItemService) { }

  ngOnInit() {
    this.itemService.subjectAllItems.subscribe(
      (items) => {
        this.items = items;
      }
    )
    // console.log(this.items);
  }

  filterItems(id_decoder){
    let printItem:Item[] = [];
    this.items.forEach(item => {
      if(item.id_decoder == id_decoder){
          return printItem.push(item); 
      }
        
    })
    if(printItem.length == 0){
      printItem.push(new Item(null,null,null,null,null,id_decoder,null,null,null,null,null,null,null,null,null));
      return printItem;
    }
    return printItem;
  }

}
