import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { AccountService } from '../service/account.service';

@Component({
  selector: 'register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  constructor(
    private formBuilder: FormBuilder,
    private accountService: AccountService) { }

  addForm: FormGroup;
  doneMessage:string;

  ngOnInit() {
    this.initForm();
    this.accountService.registerSubjectMessage.subscribe(
      (message) => {
        this.doneMessage = message;
      }
    )
  }

  initForm() {
    this.addForm = this.formBuilder.group({
      pseudo: ['', Validators.required],
      password: ['', Validators.required],
      re_password: ['', Validators.required],
    });
  }

  register(){
    const pseudo = this.addForm.get('pseudo').value;
    const password = this.addForm.get('password').value;
    const re_password = this.addForm.get('re_password').value;
    if(password === re_password){
      this.accountService.register(pseudo,password);
    }
    else{
      this.doneMessage = "Vos mots de passe ne concordent pas";
    }
  }

}
