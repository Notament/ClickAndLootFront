import { Component, OnInit, Input } from '@angular/core';
import { HeroService } from '../service/hero.service';
import { ItemService } from '../service/item.service';
import { MapService } from '../service/map.service';
import { Hero } from '../model/Hero';


@Component({
  selector: 'hero-item',
  templateUrl: './hero-item.component.html',
  styleUrls: ['./hero-item.component.scss']
})
export class HeroItemComponent implements OnInit {

  @Input() hero: Hero;

  constructor(private heroService: HeroService, private mapService: MapService, private itemService: ItemService) { }

  ngOnInit() {
    this.heroService.setHero(this.hero);
  }

  playHero(id){

    this.itemService.getAllItems(id);  
    this.mapService.getAllMaps(id);
  }

  deleteHero(id,user_id){
    this.heroService.deleteHero(id,user_id);
  }

  xpToPercentage(hero){
    return (100 * hero.current_xp) / hero.total_xp + '%';
  }

  lifeToPercentage(hero){
    return (100 * hero.health) / 100 + '%';
  }

  ressourceToPercentage(hero){
    return (100 * hero.ressource) / 100 + '%';
  }

}
