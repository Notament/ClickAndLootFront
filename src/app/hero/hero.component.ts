import { Component, OnInit } from '@angular/core';
import { AccountService } from '../service/account.service';
import { HeroService } from '../service/hero.service';
import { User } from '../model/User';
import { Hero } from '../model/Hero';

@Component({
  selector: 'hero',
  templateUrl: './hero.component.html',
  styleUrls: ['./hero.component.scss']
})
export class HeroComponent implements OnInit {

  private user: User;
  private heroes: Hero[];
  private doneMessage : string;

  constructor(
    private accountService: AccountService,
    private heroService: HeroService) { }

  ngOnInit() {

    this.accountService.userSubject.subscribe(
      (user) => {
        this.user = user;
      }
    )
    this.heroService.heroesSubject.subscribe(
      (heroes) => {
        this.heroes = heroes;
      }
    )
    this.heroService.getAllHero(this.user.id);
  }

}
