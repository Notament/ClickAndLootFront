import { Component, OnInit } from '@angular/core';
import { MapService } from '../service/map.service';
import { Map } from '../model/Map';
import { Hero } from '../model/Hero';
import { Item } from '../model/Item';
import { ItemService } from '../service/item.service';

@Component({
  selector: 'ath',
  templateUrl: './ath.component.html',
  styleUrls: ['./ath.component.scss']
})
export class AthComponent implements OnInit {

  items: Item[];
  map:Map;
  hero:Hero;
  error:string;
  isClicked:boolean;

  constructor(private mapService: MapService, private itemService: ItemService) { }

    ngOnInit() {
      this.mapService.subjectMap.subscribe(
        (map) => {
          this.map = map;
        }
      )
      this.mapService.subjectisClicked.subscribe(
        (isClicked) => {
          this.isClicked = isClicked;
        }
      )
      this.hero = this.mapService.hero;
      this.itemService.subjectAllItems.subscribe(
        (items) => {
          this.items = items;
        }
      )
    }

  moveHero(X:number,Y:number){
    let XToGo = parseInt(this.hero.hero_X) + X;
    let YToGo = parseInt(this.hero.hero_Y) + Y;
    if(this.map.cellExist(XToGo,YToGo)) {
      this.mapService.moveHeroOnGrid(XToGo,YToGo);
    }
    else {
      this.error = "Pas de case dans cette direction";
      setTimeout(() => { this.error = "" }, 1500);
    }
  }
  reducer = (accumalotor, currentValue) => accumalotor + currentValue;

  statSum(stat){
    let stats = [];
    if(this.items.length != 0) {
        this.items.forEach((item) => {
            stats.push(parseInt(item[stat]));
        })
        return Math.round(stats.reduce(this.reducer));
    }
    return 0;
  }

  xpToPercentage(hero) {
    return (100 * hero.current_xp) / hero.total_xp + '%';
  }

  lifeToPercentage(hero) {
    return (100 * hero.health) / 100 + '%';
  }

  ressourceToPercentage(hero) {
    return (100 * hero.ressource) / 100 + '%';
  }


}
